this is a template firmware for [ESP32-S2](https://www.espressif.com/en/products/socs/esp32-s2)-based micro-controller projects.

board
=====

the board used is a [WEMOS](https://www.wemos.cc/en/latest/index.html) [S2 mini](https://www.wemos.cc/en/latest/s2/s2_mini.html).

compile and flash
=================

the firmware uses [ESP-IDF](https://github.com/espressif/esp-idf) [v5.0](https://docs.espressif.com/projects/esp-idf/en/v5.0/esp32s2/index.html).
See [getting started](https://docs.espressif.com/projects/esp-idf/en/v5.0/esp32s2/get-started/linux-macos-setup.html) to install the IDF.
once installed, load the IDF tools:

~~~
get_idf
~~~

to build the firmware:

~~~
idf.py build
~~~

note that the first time it will download additional components (such as [tinyUSB](https://git.cuvoodoo.info/kingkevin/espressif_tinyusb) and [esp_tinyusb](https://git.cuvoodoo.info/kingkevin/espressif_idf-extra-components/src/branch/esp_tinyusb_dfu/usb/esp_tinyusb)).

the firmware supports [USB DFU](https://usb.org/document-library/device-firmware-upgrade-11-new-version-31-aug-2004) flashing.
it is meant to be used along the [ESP32-S2 DFU mode firmware](https://git.cuvoodoo.info/kingkevin/esp32-s2_dfu).
Once the DFU mode firmware is installed (on the `factory` partition), you can flash this firmware (on the `ota0` partition) using `dfu-util`:

~~~
dfu-util --device 1d50:617b --alt 1 --download build/main.bin --reset
~~~

if the firmware is stuck, and you can't switch from runtime mode to DFU mode using `dfu-util`, hold pin 14 high while resetting.
this will force booting the DFU mode.

you can also flash using the ESP ROM bootloader (hold GPIO0 low while resetting) and `esptool`:

~~~
esptool.py -p /dev/ttyACM0 --before no_reset --after no_reset --chip esp32s2 write_flash --flash_mode dio --flash_size detect --flash_freq 80m 0x50000 build/main.bin
~~~
